// Copyright 2019 <Copyright Chaffinch Optmization-FURG>
#ifndef ROOM_H
#define ROOM_H

#include <utility>

#include "QRect"
#include "QVector"
#include "QPoint"

#include "evoga.h"

#include "environment/wall.h"
#include "definitions/direction.h"

namespace environment {
class Room {
 public:
    Room();
    Room(QRect, farsa::Arena*, double, double,
        double, double, QVector<double>);
    virtual ~Room();
    void connect(Room*, definitions::Direction);
    void draw();
    void undraw();
    void redraw();
    int contains(farsa::RobotOnPlane* robot);
    farsa::Arena* arena() const { return arena_; }
    QRect geometry() const { return geometry_; }
    QVector<bool> doors() const { return doors_; }
    QVector<farsa::Cylinder2DWrapper*> cylinders() const { return cylinders_; }
    QVector<double> sigma() const { return sigma_; }
    QVector<QVector<QVector<farsa::Box2DWrapper*> > > floor_grid() const {
        return floor_grid_;
    }
    QVector<Wall*> walls() const { return walls_; }
    bool door(definitions::Direction dir) const { return doors_[dir]; }
    bool drawn() const { return drawn_; }
    bool enable_grid() const { return enable_grid_; }
    double thickness() const { return thickness_; }
    double z() const { return z_; }
    double door_size() const { return door_size_; }
    double cell_size() const { return cell_size_; }
    float rand_size_x() const { return rand_size_x_; }
    float rand_size_y() const { return rand_size_y_; }
    unsigned int seed_x() const { return seed_x_; }
    unsigned int seed_y() const { return seed_y_; }
    double size_w() const {
        return static_cast<double>(geometry_.width()) + rand_size_x();
    }
    double size_h() const {
        return static_cast<double>(geometry_.height()) + rand_size_y();
    }
    bool visited_floor(int, int, int);
    inline std::pair<double, double> anchor_topleft() const {
        double x = geometry().x() -
            ((static_cast<double>(geometry().width()) + rand_size_x())/2);
        double y = geometry().y() +
            ((static_cast<double>(geometry().height()) + rand_size_y())/2);
        return std::make_pair(x, y);
    }
    inline std::pair<double, double> anchor_bottomright() const {
        double x = geometry().x() +
            ((static_cast<double>(geometry().width()) + rand_size_x())/2);
        double y = geometry().y() -
            ((static_cast<double>(geometry().height()) + rand_size_y())/2);
        return std::make_pair(x, y);
    }
    void set_arena(farsa::Arena* arena) { arena_ = arena; }
    void set_geometry(QRect geometry) { geometry_ = geometry; }
    void set_doors(QVector<bool> doors) { doors_ = doors; }
    void set_cylinders(QVector<farsa::Cylinder2DWrapper*> cylinders) {
        cylinders_ = cylinders;
    }
    void set_sigma(QVector<double> sigma) { sigma_ = sigma; }
    void set_floor_grid(
        QVector<QVector<QVector<farsa::Box2DWrapper*> > > floor_grid) {
        floor_grid_ = floor_grid;
    }
    void set_walls(QVector<Wall*> walls) { walls_ = walls; }
    inline void set_door(definitions::Direction dir, bool state) {
        doors_[static_cast<int>(dir)] = state;
            if ( walls_[static_cast<int>(dir)] != nullptr ) {
                walls_[static_cast<int>(dir)]->set_door(state);
            }
    }
    inline void set_enable_grid(bool state) {
        enable_grid_ = state;
        if (drawn()) {
            redraw();
        }
    }
    void set_thickness(double thickness) { thickness_ = thickness; }
    inline void set_z(double z) {
        z_ = z;
        for ( auto &wall : walls_ ) {
            wall->set_z(z);
        }
    }
    void set_door_size(double door_size) { door_size_ = door_size; }
    void set_cell_size(double cell_size) { cell_size_ = cell_size; }
    void set_rand_size_x(float size) { rand_size_x_ = size; }
    void set_rand_size_y(float size) { rand_size_y_ = size; }
    void set_seed_x(unsigned int seed_x) { seed_x_ = seed_x; }
    void set_seed_y(unsigned int seed_y) { seed_y_ = seed_y; }
    void visite_floor(int, int, int);
    void overlap_cords(farsa::RobotOnPlane*, int, int&, int&);
    double fit();
    double total_cleaned_area();
    double total_area();

 private:
    farsa::Arena* arena_;
    QRect geometry_;
    QVector<bool> doors_;
    QVector<farsa::Cylinder2DWrapper*> cylinders_;
    QVector<double> sigma_;
    QVector<QVector<QVector<farsa::Box2DWrapper*> > > floor_grid_;
    QVector<QVector< QVector<bool> > > visited_floor_;
    QVector<QVector< QVector<double> > > grid_area_;
    QVector<Wall*> walls_;
    bool drawn_;
    bool enable_grid_;
    double thickness_;
    double z_;
    double door_size_;
    double cell_size_;
    float rand_size_x_;
    float rand_size_y_;
    unsigned int seed_x_;
    unsigned int seed_y_;
    void init_grid();
    void init_grid_hall(int, float);
    void draw_grid();
    void undraw_grid();
};
}  // namespace environment
#endif
