// Copyright 2019 <Copyright Chaffinch Optmization-FURG>
#ifndef WALL_H
#define WALL_H

#include <utility>

#include "QRect"
#include "QVector"
#include "QColor"

#include "evoga.h"

#include "definitions/direction.h"

namespace environment {
class Wall {
 public:
    Wall(QRect, std::pair<double, double>, definitions::Direction);
    Wall(std::pair<farsa::wVector, farsa::wVector>, bool);
    virtual ~Wall();
    void draw();
    void undraw();
    void redraw();
    void set_color(QColor);
    void set_door(bool);
    void set_door_size(float);
    void set_arena(farsa::Arena*);
    void set_z(double);
    void set_thickness(double);
    QColor color();
    bool door();
    bool drawn();
    double z();
    double thickness();
    float door_size();
    QVector<farsa::Box2DWrapper*> box_2D();
    QVector<farsa::wVector> points();
    farsa::Arena* arena();

 private:
    QColor color_;
    bool door_;
    bool drawn_;
    QVector<farsa::wVector> points_;
    float base_w;
    float base_h;
    float cx;
    float cy;
    float door_size_;
    double thickness_;
    double z_;
    int TYPE_;
    definitions::Direction dir_;
    QVector<farsa::Box2DWrapper*> box_2D_;
    std::pair<farsa::wVector, farsa::wVector> target_;
    farsa::Arena* arena_;
    void make_points();
};
}  // namespace environment
#endif
