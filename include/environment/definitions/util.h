// Copyright 2019 <Copyright Chaffinch Optmization-FURG>
#ifndef UTIL_H
#define UTIL_H

#include <utility>

namespace environment {
namespace definitions {
enum Overlap{
    NO_OVERLAP = -2,
    OVERLAP = -1,
};
}  // namespace definitions
}  // namespace environment
#endif
