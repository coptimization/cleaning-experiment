// Copyright 2019 <Copyright Chaffinch Optmization-FURG>
#ifndef DIRECTION_H
#define DIRECTION_H
namespace environment {
namespace definitions {
enum Direction{
    BOTTOM,
    TOP,
    LEFT,
    RIGHT
};
}  // namespace definitions
}  // namespace environment
#endif
