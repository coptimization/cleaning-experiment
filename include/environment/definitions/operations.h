// Copyright 2019 <Copyright Chaffinch Optmization-FURG>
#ifndef OPERATIONS_H
#define OPERATIONS_H

#include <utility>

namespace environment {
namespace definitions {
    bool overlap(long double, long double, long double,
                    long double, long double, long double);
    bool overlap(std::pair<double, double>, std::pair<double, double>,
                std::pair<double, double>, std::pair<double, double>);
}  // namespace definitions
}  // namespace environment
#endif
