/********************************************************************************
 *  FARSA - Total99                                                             *
 *  Copyright (C) 2012-2013 Gianluca Massera <emmegian@yahoo.it>                *
 *                                                                              *
 *  This program is free software; you can redistribute it and/or modify        *
 *  it under the terms of the GNU General Public License as published by        *
 *  the Free Software Foundation; either version 2 of the License, or           *
 *  (at your option) any later version.                                         *
 *                                                                              *
 *  This program is distributed in the hope that it will be useful,             *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 *  GNU General Public License for more details.                                *
 *                                                                              *
 *  You should have received a copy of the GNU General Public License           *
 *  along with this program; if not, write to the Free Software                 *
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA  *
 ********************************************************************************/

#include <QList>

#include "marxbotsensors.h"
#include "marxbotmotors.h"
#include "robots.h"
#include "motors.h"
#include "wheeledexperimenthelper.h"
#include "resourcesuser.h"
#include "farsaplugin.h"
#include "wmesh.h"

/**
 * \brief The motor controlling the velocity of the wheels of the MarXbot
 *
 * The motor controlling the velocity of the wheels of the MarXbot. This motor
 * applies noise if requested.
 *
 * This class does not add any other parameter to those defined by the parent
 * class (MarXbotMotor)
 *
 * The resources required by this Motor are the same as those of the parent
 * class
 */
class FARSA_PLUGIN_API MarXbotWheelVelocityMotorMod:
    public farsa::MarXbotMotor {
    FARSA_REGISTER_CLASS(MarXbotMotor)

 public:
    /**
     * \brief Constructor
     *
     * Creates and configures the motor
     * \param params the ConfigurationParameters containing the parameters
     * \param prefix the path prefix to the paramters for this Sensor
     */
    MarXbotWheelVelocityMotorMod(farsa::ConfigurationParameters& params,
                                                            QString prefix);
    /**
     * \brief Destructor
     */
    virtual ~MarXbotWheelVelocityMotorMod();
    /**
     * \brief Saves current parameters into the given
     *        ConfigurationParameters object
     *
     * \param params the ConfigurationParameters object in which parameters
     *               should be saved
     * \param prefix the name and path of the group where to save parametrs
     */
    virtual void save(farsa::ConfigurationParameters& params,
                                                    QString prefix);
    /**
     * \brief Generates a description of this class and its parameters
     *
     * \param type the string representation of this class name
     */
    static void describe(QString type);
    /**
     * \brief Performs the motor update
     */
    virtual void update();
    /**
     * \brief Returns the number of neurons required by this motor
     *
     * \return the number of neurons required by this motor
     */
    virtual int size();
 private:
    /**
     * \brief The function called when a resource used here is changed
     *
     * \param resourceName the name of the resource that has changed.
     * \param chageType the type of change the resource has gone through
     *                  (whether it was created, modified or deleted)
     */
    virtual void resourceChanged(QString resourceName,
                            ResourceChangeType changeType);
    /**
     * \brief The robot to use
     */
    farsa::PhyMarXbot* m_robot;
    /**
     * \brief The object to iterate over neurons of the neural network
     */
    farsa::NeuronsIterator* m_neuronsIterator;
    /**
     * \brief The motor mode
     */
    int motormode;
    /**
     * \brief Number of output neurons
     */
    int m_numOutput;
    bool direction = true;
};
