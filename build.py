import os
import sys
from lib.farsagstyle import make


if __name__ == "__main__":
    assert len(sys.argv) > 1, "Erro deve ser especificado o tipo de build!!!"
    pwd = os.path.dirname(os.path.abspath(__file__))
    assert os.path.isdir( os.path.join(pwd, 'build')), "O projeto deve conter uma pasta build"
    build_type = sys.argv[1]
    assert build_type in ["debug", "release", "debug_win"], "Tipo de build invalido!!"
    flags = [] if len(sys.argv) < 2 else sys.argv[3:]
    make(pwd, os.path.join(pwd, os.path.join("./build/", build_type)), flags)