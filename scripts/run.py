import os
import sys
import subprocess

# In this section is defined variables necessary for further simulation.
# The variable 'steps' serves to indicate minimum value of 'steps' that the 
# simulation will have, ie how many cycles will she perform.
# The variables 'functUp' and 'functDown' only indicate what kind of functions will be
# used to increment or decrement the clock.

steps = 50000
functUp = 'linear'
functDown = 'linear'
simulation = "temporalE2Release"
path_base = os.path.join(os.path.abspath(os.path.dirname(__file__)), '..')
dirs = ['expo', 'sub_e', 'sub_l', 'linear']
workdir = ''
threshold = 0.95

# The variables 'simulation' and 'dirs' are directories that will be used later
# to get the configuration file and save the simulation data respectively.
# Variable 'threshold' is used to select which simulations will have your route saved.

# Is made a for in the dirs list and checked if it already exists,
# otherwise one with the same name is created.

for _dir in dirs:
    d = os.path.join(path_base, 'data', _dir)
    if not os.path.isdir(d):
        os.mkdir(d)

# Here is main for loop, where it will be called the Farsa
# until the number of steps reaches 300 thousand,
# with a logic of always keep a proportion 
# in 'stepsDown' and 'stepsUp' of 95% and 5% respectively.

os.chdir("../")
while steps < 300000:
    stepsDown = int((steps/5)*.95)
    stepsUp = int((steps/5)*.05) 
    tr = os.path.join(path_base, 'data', 'trial_info.csv')
    prb = os.path.join(path_base, 'data', 'position_robot.csv')
    if os.path.isfile(tr):
        os.remove(tr)
    if os.path.isfile(prb):
        os.remove(prb)

    sys.stdout.writelines("\r Simulando ({0}:{2}) -> ({1}:{3}) com {4} steps".format(functUp,
                                                                               stepsUp, functDown, stepsDown, steps))
    # After checking the files that will be used to save the data
    # the Farsa program call is started, to work this way was included
    # in the configuration.ini file some previous information and because of that we can change
    # via the command line the variables we want made by the command "-PComponent / GA / Experiment / ..."
    # after adjusting the variables the call is made and the program displays the characteristics of the current simulation
    # to be running in the background
    sys.stdout.flush()
    action_cmd = ["-PComponent/GA/Experiment/nsteps={}".format(steps),
                  "-PComponent/GA/Experiment/fk_raise_function={}".format(
                      functUp),
                  "-PComponent/GA/Experiment/fk_fall_function={}".format(
                      functDown),
                  "-PComponent/GA/Experiment/fk_steps_raise={}".format(
                      stepsUp),
                  "-PComponent/GA/Experiment/fk_steps_fall={}".format(
                      stepsDown),
                  "-PComponent/GA/Experiment/threshold={}".format(threshold)]
    cmd_args = ["total99", "--batch", "--file=configuration.ini",
                "--action=runTest", *action_cmd]
    os.chdir("simulations/{}".format(simulation))
    proc = subprocess.Popen(
        cmd_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, error = proc.communicate()
    
    # At the end of the simulation the data files are reallocated according to the function used
    # and is renamed for its features
    
    if error.decode("utf-8") == '':
        os.chdir("../../data")
        if functUp == 'exponential':
            if functDown == 'exponential':
                workdir = 'expo'
            else:
                workdir = 'sub_e'
        elif functUp == 'linear':
            if functDown == 'exponential':
                workdir = 'sub_l'
            else:
                workdir = 'linear'
        os.rename("trial_info.csv",
                    "{}/info_s{}sup{}sdown{}.csv".format(workdir, steps, stepsUp, stepsDown))
        os.rename(
            os.path.join("position_robot.csv"),
            os.path.join(workdir, "trage_s{}sup{}sdown{}.csv".format(
                steps, stepsUp, stepsDown))
        )

    else:
        print("Erro ao executar o experimento. Parametros:\n {} -> {}\n {} -> {}\n Steps {}".format(
            functDown, stepsUp, functDown, stepsDown, steps))
    steps += 5000
    os.chdir("../")
