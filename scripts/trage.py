import os
import csv
import subprocess
import pandas as pd
import numpy as np

from io import StringIO
from matplotlib import pyplot as plt

path_base = os.path.join(os.path.abspath(
    os.path.dirname(__file__)), "../data/")

paths = [os.path.join(path_base, d) for d in os.listdir(
    path_base) if os.path.isdir(os.path.join(path_base, d))]

if not os.path.isdir(os.path.join(path_base, 'plot')):
    try:
        os.mkdir(os.path.join(path_base, 'plot'))
    except:
        raise Exception("Erro ao criar pasta  plot/!")

if not os.path.isdir(os.path.join(path_base, 'plot/trage')):
    try:
        os.mkdir(os.path.join(path_base, 'plot/trage'))
    except:
        raise Exception("Erro ao criar pasta  plot/trage!")

mode = {
    'expo': 'Exponencial Sub. e Desc.',
    'linear': 'Linear Sub. e Desc.',
    'sub_l': 'Sub. Linear. Desc. Exponencial.',
    'sub_e': 'Sub. Exponencial. Desc. Exponencial.'
}

font_1 = {'family': 'serif', 'color': '#5C1BCC', 'size': '14'}

threshold = 0.75
size_ms = 1
colors = ['red', 'blue', 'gray', 'magenta', 'green']
chunks = 5
steps = 280000
stepsDown = 51500

for path in paths:
    trials = np.array(
        sorted([f for f in os.listdir(path) if f.endswith(".csv") and f.startswith('info')]))

    for info, trage in map(lambda e: (e, e.replace('info', 'trage')), trials):
        infodf = pd.read_csv(os.path.join(path_base, path, info), sep=';')
        steps, stepsUp, stepsDown = list(map(
            int, info.split('info_s')[1].split('.csv')[0].replace('up', '').replace('down', '').split('s')))
        with open(os.path.join(path, trage)) as pointer:
            trage_plain = pointer.read()
            trages = list(filter(lambda e: len(e) > 1, trage_plain.split("=")))
            ids_best = infodf[infodf['total_fitness']
                              >= threshold].index.to_list()
            for id_trial, trag in zip(ids_best, trages):
                dataset = pd.read_csv(StringIO(trag), sep=";")
                times = dataset['x'].size // chunks
                fig, ax = plt.subplots(figsize=(10, 10))
                labels, lines = [], []
                total_fitness = infodf.iloc[id_trial]['total_fitness']*100
                ax.set_title(
                    'Posição do Robô (Trial {}) - {:.2f}% limpo.'.format(id_trial+1, total_fitness))
                for it in range(chunks):
                    batch = dataset[['x', 'y']].loc[it*times:(it+1)*times]
                    ax.scatter(batch['x'], batch['y'],
                               c=colors[it], s=0.1, alpha=1)
                    lines.append(plt.Line2D([0], [0], color=colors[it], lw=4))
                    labels.append("step $\exists$ [{},{}]".format(
                        it*times, (it+1)*times))
                ax.legend(lines, labels,  bbox_to_anchor=(0, -0.1, 1, 1))
                plt.savefig(os.path.join(path_base, 'plot/trage',
                                         'trage_{}_s_{}_up_{}_down_{}_n_{}.png'.format(os.path.basename(path), steps,
                                                                                       stepsUp, stepsDown, id_trial+1)
                                         ), dpi=100)
