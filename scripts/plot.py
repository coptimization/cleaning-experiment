import os
import sys

import numpy as np
import pandas as pd

from matplotlib import pyplot as plt

path_base = os.path.join(os.path.abspath(
    os.path.dirname(__file__)), "../data/")

paths = [os.path.join(path_base, d) for d in os.listdir(
    path_base) if os.path.isdir(os.path.join(path_base, d))]

if not os.path.isdir(os.path.join(path_base, 'plot')):
    try:
        os.mkdir(os.path.join(path_base, 'plot'))
    except:
        raise Exception("Erro ao criar pasta Plot!")


mode = {
    'expo': 'Exponencial Sub. e Desc.',
    'linear': 'Linear Sub. e Desc.',
    'sub_l': 'Sub. Linear. Desc. Exponencial.',
    'sub_e': 'Sub. Exponencial. Desc. Exponencial.'
}

font_1 = {'family': 'serif', 'color': '#5C1BCC', 'size': '14'}

for path in paths:
    trials = np.array(
        sorted([f for f in os.listdir(path) if f.endswith(".csv") and f.startswith('info')]))
    for id_trial in range(0, len(trials), 3):
        sys.stdout.write("\r -> Salvando Plots ({}/{}) Modo ({})".format(
            id_trial+3 if id_trial +
            3 < len(trials) else len(trials), len(trials),
            os.path.basename(path)
        ))
        sys.stdout.flush()

        # foreach trials 3n
        r = list(range(id_trial, id_trial+3 if id_trial +
                       3 <= len(trials) else len(trials)))
        batch_trials = trials[r]
        batch_infos = list(map(lambda e: pd.read_csv(
            os.path.join(path, e), sep=";"), batch_trials))
        values_fit = list(map(lambda e: e['total_fitness'], batch_infos))
        stati_data = list(
            map(lambda e: e['total_fitness'].describe(), batch_infos))
        labels = list(map(lambda e: '{} - steps({})\n up({}) down({})'.format(os.path.basename(path).capitalize(), *e.replace(
            '_s', '_$').replace('sdown', '$').replace('sup', '$').split('_')[1].split('.csv')[0].split('$')[1:]), batch_trials))
        plt.figure(figsize=(25, 15))
        plt.boxplot([*values_fit],
                    labels=labels)
        plt.xticks([1, 2, 3], labels)
        plt.title(
            'Boxplot Fitness - Modo {}'.format(mode[os.path.basename(path)]))
        plt.ylabel('% de Limpeza')
        plt.xlabel('Tempo de Descida')
        for i in range(len(labels)):
            plt.text(i+1.2, stati_data[i]['25%'],
                     "{:.2f}".format(stati_data[i]['25%']), fontdict=font_1)
            plt.text(i+1.2, stati_data[i]['50%'],
                     '{:.2f}'.format(stati_data[i]['50%']), fontdict=font_1)
            plt.text(i+1.2, stati_data[i]['75%'],
                     '{:.2f}'.format(stati_data[i]['75%']), fontdict=font_1)

        plt.savefig(os.path.join(path_base,
                                 'plot/{}_{}_{}_boxplot.png'.format(os.path.basename(path).capitalize(),
                                                                    id_trial, id_trial+3)))
print()
