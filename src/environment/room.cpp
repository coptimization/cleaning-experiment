// Copyright 2019 <Copyright Chaffinch Optmization-FURG>
#include <iostream>
#include <utility>

#include "environment/room.h"
#include "environment/definitions/operations.h"
#include "environment/definitions/util.h"

namespace environment {
Room::Room() {}
Room::Room(QRect geometry, farsa::Arena*arena,
    double thickness, double z, double cell_size,
    double door_size, QVector<double> sigma):
        thickness_(thickness),
        z_(z),
        sigma_(sigma),
        geometry_(geometry),
        arena_(arena),
        cell_size_(cell_size),
        enable_grid_(false),
        drawn_(false),
        doors_({false, false, false, false}),
        door_size_(door_size) {
    seed_x_ = rand_r(new unsigned int);
    seed_y_ = rand_r(new unsigned int);
    rand_size_x_ = (rand_r(&seed_x_) % static_cast<int>(100*sigma[0]))/100.0;
    rand_size_y_ = (rand_r(&seed_y_) % static_cast<int>(100*sigma[1]))/100.0;
    walls_ = QVector<Wall*>(12, nullptr);
    for ( int j = 0; j < 4; j++ ) {
        walls_[j] = new Wall(geometry, std::make_pair(rand_size_x(),
            rand_size_y()), static_cast<definitions::Direction>(j));
        walls_[j]->set_thickness(thickness);
        walls_[j]->set_z(z);
        walls_[j]->set_arena(arena);
        walls_[j]->set_door_size(door_size_);
    }
    init_grid();
}
void Room::init_grid() {
    floor_grid_.resize(5);
    visited_floor_.resize(5);
    grid_area_.resize(5);
    double size_j, size_i;
    size_j = static_cast<double>(geometry().width())  + rand_size_x();
    size_i = static_cast<double>(geometry().height()) + rand_size_y();
    int central_floor_j, central_floor_i;
    if (fmod(size_j, cell_size()) > 1e-4) {
        central_floor_j = ceil(size_j/cell_size());
    } else {
        central_floor_j = size_j/cell_size();
    }
    if (fmod(size_i, cell_size()) > 1e-4) {
        central_floor_i = ceil(size_i/cell_size());
    } else {
        central_floor_i = size_i/cell_size();
    }
    floor_grid_[0].resize(central_floor_i);
    visited_floor_[0].resize(central_floor_i);
    grid_area_[0].resize(central_floor_i);
    for ( int i = 0; i < floor_grid_[0].size(); i++ ) {
        floor_grid_[0][i].resize(central_floor_j);
        visited_floor_[0][i].resize(central_floor_j);
        grid_area_[0][i].resize(central_floor_j);
        for ( int j = 0; j < central_floor_j; j++ ) {
            visited_floor_[0][i][j] = false;
            floor_grid_[0][i][j] = nullptr;
            grid_area_[0][i][j] = 0;
        }
    }
    for ( int hall = 1; hall < 5; hall++ ) {
        floor_grid_[hall].resize(0);
        visited_floor_[hall].resize(0);
        grid_area_[hall].resize(0);
    }
}
void Room::init_grid_hall(int hall, float size) {
    if (floor_grid_[hall].size() == 0) {
        int x, y, dir;
        float div_c, div_s;
        dir = hall-1;
        div_c = door_size()/cell_size();
        div_s = size/cell_size();
        if (dir == definitions::LEFT || dir == definitions::RIGHT) {
            if (fmod(size, cell_size()) > 1e-4) {
                x = ceil(div_s);
            } else {
                x = static_cast<int>(div_s);
            }
            if (fmod(door_size(), cell_size()) > 1e-4) {
                y = ceil(div_c);
            } else {
                y = static_cast<int>(div_c);
            }
        } else {
            if (fmod(size, cell_size()) > 1e-4) {
                y = ceil(div_s);
            } else {
                y = static_cast<int>(div_s);
            }
            if (fmod(door_size(), cell_size()) > 1e-4) {
                x = ceil(div_c);
            } else {
                x = static_cast<int>(div_c);
            }
        }
        floor_grid_[hall].resize(y);
        visited_floor_[hall].resize(y);
        grid_area_[hall].resize(y);
        for (int i = 0; i < floor_grid_[hall].size(); i++) {
            floor_grid_[hall][i].resize(x);
            visited_floor_[hall][i].resize(x);
            grid_area_[hall][i].resize(x);
            for (int j = 0; j < x; j++) {
                visited_floor_[hall][i][j] = false;
                floor_grid_[hall][i][j] = nullptr;
                grid_area_[hall][i][j] = 0;
            }
        }
    }
}
void Room::draw_grid() {
    double rest_x, rest_y, cx, cy;
    bool draw_x, draw_y;
    for (int draw_step = 0; draw_step < 5; draw_step++) {
        if (draw_step == 0) {
            rest_x = fmod(size_w(), cell_size());
            rest_y = fmod(size_h(), cell_size());
            cx = anchor_topleft().first;
            cy = anchor_topleft().second;
        } else {
            int dir = draw_step-1;
            int wall = 4 + 2*dir;
            if (walls_[wall] != nullptr) {
                double size;
                if ( dir == definitions::LEFT || dir == definitions::RIGHT ) {
                    size = abs(walls_[wall]->points()[1][0] -
                                walls_[wall]->points()[0][0]);
                    if (dir == definitions::RIGHT) {
                        cx = anchor_bottomright().first;
                    } else {
                        cx = walls_[wall]->points()[1][0];
                    }
                    cy = geometry_.y() + (door_size()/2);
                    rest_x = fmod(size, cell_size());
                    rest_y = fmod(door_size(), cell_size());
                } else {
                    size = abs(walls_[wall]->points()[1][1] -
                                walls_[wall]->points()[0][1]);
                    if (dir == definitions::TOP) {
                        cy = walls_[wall]->points()[1][1];
                    } else {
                        cy = anchor_bottomright().second;
                    }
                    cx = geometry_.x() - (door_size()/2);
                    rest_y = fmod(size, cell_size());
                    rest_x = fmod(door_size(), cell_size());
                }
            }
        }
        draw_x = rest_x > 1e-4;
        draw_y = rest_y > 1e-4;
        for (int i = 0; i < floor_grid_[draw_step].size(); i++) {
            for (int j = 0; j < floor_grid_[draw_step][i].size(); j++) {
                double cx_, cy_, cell_w, cell_h;
                cell_w = cell_size();
                cell_h = cell_size();
                cx_ = cx + j*cell_w + (cell_w/2.0);
                cy_ = cy - i*cell_h - (cell_h/2.0);
                if (i == floor_grid_[draw_step].size()-1 && draw_y) {
                    cy_ += (cell_h - rest_y)/2;
                    cell_h = rest_y;
                }
                if (j == floor_grid_[draw_step][i].size()-1 && draw_x) {
                    cx_-= (cell_w - rest_x)/2;
                    cell_w = rest_x;
                }
                if (enable_grid()) {
                    floor_grid_[draw_step][i][j] =
                        arena()->createRectangularTargetArea(
                            cell_w, cell_h, Qt::black);
                            floor_grid_[draw_step][i][j]->setPosition(
                            cx_, cy_);
                }
                if (cell_w*cell_h >= pow(cell_size()/2, 2)) {
                    grid_area_[draw_step][i][j] = cell_w*cell_h;
                } else {
                    grid_area_[draw_step][i][j] = 0;
                }
            }
        }
    }
}
void Room::undraw_grid() {
    for (int step=0; step < floor_grid_.size(); step++) {
        for (int i = 0; i < floor_grid_[step].size(); i++) {
            for (int j = 0; j < floor_grid_[step][i].size(); j++) {
                if (floor_grid_[step][i][j] != nullptr) {
                    arena()->delete2DObject(floor_grid_[step][i][j]);
                    grid_area_[step][i][j] = 0;
                }
            }
        }
    }
}
int Room::contains(farsa::RobotOnPlane* robot) {
    double x, y;
    x = robot->position().x;
    y = robot->position().y;
    std::pair<double, double> robot_topleft({x - robot->robotRadius(),
        y + robot->robotRadius()});
    std::pair<double, double> robot_bottomright({x + robot->robotRadius(),
        y - robot->robotRadius()});
    if (definitions::overlap(anchor_topleft(), anchor_bottomright(),
        robot_topleft, robot_bottomright)) {
        return definitions::OVERLAP;
    }
    int i = 4;
    while (i <= walls_.size()-2) {
        Wall* upper = walls_[i];
        Wall* lower = walls_[i+1];
        if ( upper != nullptr && lower != nullptr ) {
            std::pair<double, double> topleft;
            std::pair<double, double> bottomright;
            int dir, mode;
            dir = (i-4)/2;
            if (dir == definitions::LEFT || dir == definitions::RIGHT) {
                mode = (dir == definitions::LEFT) ? 1 : 0;
                topleft = std::make_pair(upper->points()[mode][0],
                                        upper->points()[mode][1]);
                bottomright = std::make_pair(lower->points()[!mode][0],
                                                lower->points()[!mode][1]);
            } else {
                mode = (dir == definitions::BOTTOM) ? 0 : 1;
                topleft = std::make_pair(lower->points()[mode][0],
                                            lower->points()[mode][1]);
                bottomright = std::make_pair(upper->points()[!mode][0],
                                            upper->points()[!mode][1]);
            }
            if (definitions::overlap(topleft, bottomright,
                                        robot_topleft, robot_bottomright)) {
                return dir;
            }
        }
        i+=2;
    }
    return definitions::NO_OVERLAP;
}
bool Room::visited_floor(int x, int y, int hall) {
    if (x < visited_floor_[hall].size()) {
        if (y < visited_floor_[hall][x].size()) {
            return visited_floor_[hall][x][y];
        }
    }
    return false;
}
void Room::visite_floor(int x, int y, int hall) {
    if (x < visited_floor_[hall].size()) {
        if (y < visited_floor_[hall][x].size()) {
            visited_floor_[hall][x][y] = true;
        }
    }
}
void Room::overlap_cords(farsa::RobotOnPlane* robot,
                            int hall, int& cx, int& cy) {
    double x, y;
    double cx_, cy_;
    x = robot->position().x;
    y = robot->position().y;
    if (hall == definitions::OVERLAP) {
        cy = static_cast<int>(abs(anchor_topleft().first -
                                    robot->position().x) / cell_size());
        cx = static_cast<int>(abs(anchor_topleft().second -
                                    robot->position().y) / cell_size());
    } else {
        Wall* ref = walls_[4 + hall*2];
        if (ref != nullptr) {
            if (hall == definitions::LEFT || hall == definitions::RIGHT) {
                if (hall == definitions::RIGHT) {
                    cy_ = anchor_bottomright().first;
                } else {
                    cy_ = ref->points()[1][0];
                }
                cy = abs((cy_ - robot->position().x)/ cell_size());
                cx_ = geometry_.y() + (door_size()/2);
                cx = abs((cx_ - robot->position().y)/ cell_size());
            } else {
                if (hall == definitions::TOP) {
                    cx_ = ref->points()[1][1];
                } else {
                    cx_ = anchor_bottomright().second;
                }
                cy_ = geometry_.x() - (door_size()/2);
                cy = abs(cy_ - robot->position().x)/cell_size();
                cx = abs(cx_ - robot->position().y)/cell_size();
            }
        }
    }
}
void Room::draw() {
    if ( !drawn() ) {
        for ( auto &wall : walls() ) {
            if ( wall != nullptr ) {
                wall->draw();
                for (auto point : wall->points()) {
                    cylinders_.append(
                        arena()->createSmallCylinder(Qt::blue, z()));
                    cylinders_.back()->setStatic(true);
                    cylinders_.back()->setPosition(point.x, point.y);
                }
            }
        }
        draw_grid();
        drawn_ = true;
    }
}
void Room::undraw() {
    if (drawn()) {
        for (auto &wall : walls()) {
            if (wall != nullptr) {
                wall->undraw();
            }
        }
        for (auto cylinder : cylinders_) {
            arena()->delete2DObject(cylinder);
        }
        undraw_grid();
        cylinders_.resize(0);
        drawn_ = false;
    }
}
void Room::redraw() {
    if (drawn()) {
        undraw();
    }
    draw();
}
void Room::connect(Room *target, definitions::Direction dir) {
    QVector<farsa::wVector> target_points, points;
    definitions::Direction target_dir;
    switch (dir) {
        case definitions::LEFT:
            target_dir = definitions::RIGHT;
            break;
        case definitions::RIGHT:
            target_dir = definitions::LEFT;
            break;
        case definitions::TOP:
            target_dir = definitions::BOTTOM;
            break;
        case definitions::BOTTOM:
            target_dir = definitions::TOP;
            break;
    }
    int i, target_i;
    i = static_cast<int>(dir);
    target_i = static_cast<int>(target_dir);
    QVector<Wall*> target_walls = target->walls();
    target->set_door(target_dir, true);
    set_door(dir, true);
    target_points = target->walls()[target_i]->points();
    points = walls_[i]->points();
    if (walls()[ i+4 ] == nullptr
        && target_walls[ target_i+4 ] == nullptr) {
        std::pair<farsa::wVector, farsa::wVector> points_upper, points_lower;
        points_lower = std::make_pair(points[1], target_points[1]);
        points_upper = std::make_pair(points[2], target_points[2]);
        Wall* wall_upper = new Wall(points_upper, false);
        Wall* wall_lower = new Wall(points_lower, false);
        wall_upper->set_arena(arena_);
        wall_upper->set_thickness(thickness_);
        wall_upper->set_z(z_);
        wall_lower->set_arena(arena_);
        wall_lower->set_thickness(thickness_);
        wall_lower->set_z(z_);
        wall_lower->set_color(Qt::blue);
        walls_[ 4 + 2*i ] = wall_upper;
        target_walls[ 4 + 2*target_i ] = wall_upper;
        walls_[ 5 + 2*i ] = wall_lower;
        target_walls[ 5 + 2*target_i ] = wall_lower;
        target->set_walls(target_walls);
        if (dir == definitions::LEFT || dir == definitions::RIGHT) {
            init_grid_hall(dir+1, abs(points[1][0] - target_points[0][0]));
        } else {
            init_grid_hall(dir+1, abs(points[1][1] - target_points[0][1]));
        }
        if (drawn()) {
            redraw();
        }
    }
}
double Room::total_cleaned_area() {
    double fitness = 0;
    for (int hall=0; hall < visited_floor_.size(); hall++) {
        for (int i=0; i < visited_floor_[hall].size(); i++) {
            for (int j = 0; j < visited_floor_[hall][i].size(); j++) {
                if (visited_floor_[hall][i][j]) {
                    fitness += grid_area_[hall][i][j];
                }
            }
        }
    }
    return fitness;
}
double Room::total_area() {
    double area = 0;
    for (int hall=0; hall < visited_floor_.size(); hall++) {
        for (int i=0; i < visited_floor_[hall].size(); i++) {
            for (int j = 0; j < visited_floor_[hall][i].size(); j++) {
                area += grid_area_[hall][i][j];
            }
        }
    }
    return area;
}
double Room::fit() {
    return total_cleaned_area()/total_area();
}
Room::~Room() {
    walls_.resize(0);
    cylinders_.resize(0);
    floor_grid_.resize(0);
    visited_floor_.resize(0);
    grid_area_.resize(0);
    for (int i = 0; i < floor_grid_.size(); i++ ) {
        for (int j = 0; j < floor_grid_[i].size(); j ++) {
            floor_grid_[i][j].resize(0);
        }
        floor_grid_[i].resize(0);
    }
    for (int i = 0; i < visited_floor_.size(); i++ ) {
        for (int j = 0; j < visited_floor_[i].size(); j ++) {
            visited_floor_[i][j].resize(0);
        }
        visited_floor_[i].resize(0);
    }
    for (int i = 0; i < grid_area_.size(); i++ ) {
        for (int j = 0; j < grid_area_[i].size(); j ++) {
            grid_area_[i][j].resize(0);
        }
        grid_area_[i].resize(0);
    }
//QVector<QVector<QVector<farsa::Box2DWrapper*> > > floor_grid_;
//QVector<QVector< QVector<bool> > > visited_floor_;
//    QVector<QVector< QVector<double> > > grid_area_;

    sigma_.clear();
    doors_.clear();
    arena_ = nullptr;
}
}  // namespace environment
