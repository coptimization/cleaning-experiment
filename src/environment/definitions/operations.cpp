// Copyright 2019 <Copyright Chaffinch Optmization-FURG>
#include <iostream>

#include "environment/definitions/operations.h"

namespace environment {
namespace definitions {
bool overlap(long double x, long double y,
                long double lower_x, long double lower_y,
                long double upper_x, long double upper_y) {
        return ((x - lower_x) <= (upper_x - lower_x) &&
                (y - lower_y) <= (upper_y - lower_y));
    }
bool overlap(std::pair<double, double> lower_1,
                std::pair<double, double> upper_1,
                std::pair<double, double> lower_2,
                std::pair<double, double> upper_2) {
    if (lower_1.first > upper_2.first || lower_2.first > upper_1.first) {
        return false;
    }
    if (lower_1.second < upper_2.second || lower_2.second < upper_1.second) {
        return false;
    }
    return true;
    }
}  // namespace definitions
}  // namespace environment
