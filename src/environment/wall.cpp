// Copyright 2019 <Copyright Chaffinch Optmization-FURG>

#include <utility>

#include "environment/wall.h"


namespace environment {
Wall::Wall(QRect geometry, std::pair<double, double> rands,
        definitions::Direction dir) :
        dir_(dir),
        door_(false),
        door_size_(0.5),
        TYPE_(1),
        drawn_(false),
        color_(Qt::red) {
    base_w = geometry.width() + rands.first;
    base_h = geometry.height() + rands.second;
    cx  = geometry.x();
    cy = geometry.y();
}
Wall::Wall(std::pair<farsa::wVector, farsa::wVector> points, bool door) :
    target_(points),
    door_(door),
    door_size_(0.5),
    TYPE_(0),
    drawn_(false),
    color_(Qt::red) {}
QColor Wall::color() { return color_; }
QVector<farsa::Box2DWrapper*> Wall::box_2D() { return box_2D_; }
QVector<farsa::wVector> Wall::points() { return points_; }
bool Wall::door() { return door_; }
bool Wall::drawn() { return drawn_; }
float Wall::door_size() { return door_size_; }
farsa::Arena* Wall::arena() { return arena_; }
double Wall::z() { return z_; }
double Wall::thickness() { return thickness_; }
void Wall::set_color(QColor color) { color_ = color; }
void Wall::set_door(bool door) {
    door_ = door;
    make_points();
}
void Wall::set_door_size(float size) {
    door_size_ = size;
    make_points();
}
void Wall::set_arena(farsa::Arena* arena) { arena_ = arena; }
void Wall::set_z(double z) { z_ = z; }
void Wall::set_thickness(double thickness) { thickness_ = thickness; }
void Wall::make_points() {
        QVector<farsa::wVector> temp;
        if (TYPE_) {
            double xi, xf, yi, yf, xmdi,
                    xmdf, ymdi, ymdf;
            if ( dir_ == definitions::TOP || dir_ == definitions::BOTTOM ) {
                yi = yf = ymdf = ymdi = cy +
                (base_h/2)*(dir_ == definitions::TOP ? 1 : -1);
                xi = cx -(base_w/2);
                xf = cx +(base_w/2);
                xmdi = ((xi+xf)/2.0) - (door_size()/2.0);
                xmdf = ((xi+xf)/2.0) + (door_size()/2.0);
            } else {
                xi = xf = xmdi = xmdf = cx +
                (base_w/2)*(dir_ == definitions::RIGHT ? 1 : -1);
                yi = cy -(base_h/2);
                yf = cy +(base_h/2);
                ymdi = ((yi+yf)/2.0) - (door_size()/2.0);
                ymdf = ((yi+yf)/2.0) + (door_size()/2.0);
            }
            farsa::wVector point_i(xi, yi, 0 , 1);
            temp.append(point_i);
            if (door()) {
                farsa::wVector point_mdi(xmdi, ymdi, 0 , 1);
                farsa::wVector point_mdf(xmdf, ymdf, 0 , 1);
                temp.append(point_mdi);
                temp.append(point_mdf);
            }
            farsa::wVector point_f(xf, yf, 0 , 1);
            temp.append(point_f);
        } else {
            temp.append(target_.first);
            temp.append(target_.second);
        }
        points_  = temp;
}
void Wall::draw() {
    if ( !drawn() ) {
        make_points();
        QVector<farsa::wVector> to_draw = points();
        QVector<farsa::Box2DWrapper*> temp;
        while (to_draw.size() > 0) {
            farsa::wVector pi = to_draw.front();
            to_draw.pop_front();
            farsa::wVector pf = to_draw.front();
            to_draw.pop_front();
            farsa::Box2DWrapper* wall;
            wall = arena()->createWall(
                color_, pi, pf,
                thickness(), z());
            temp.append(wall);
        }
        box_2D_ = temp;
        drawn_ = true;
    }
}
void Wall::undraw() {
    if ( drawn() ) {
        for (auto obj_2d : box_2D()) {
            arena()->delete2DObject(obj_2d);
        }
        box_2D_.clear();
        drawn_ = false;
    }
}
Wall::~Wall() {
    points_.resize(0);
    box_2D_.resize(0);
    arena_ = nullptr;
}
}  // namespace environment
