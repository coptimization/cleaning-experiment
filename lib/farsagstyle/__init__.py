import os
import sys
import re
import glob
import subprocess

def make(source_dir, build_dir, make_flags=['']):
  with open(os.path.join(source_dir,"CMakeLists.txt")) as pointer:
    payload = pointer.read()
    pattern = re.compile('^project\((.*)\)$', re.MULTILINE)
    res = pattern.findall(payload)
    assert len(res) > 0, "Erro, Nome do projeto não definido!!!!"
    project_name = res[0]
    assert os.path.isdir(build_dir), "Diretorio Invalido de Compilação!!!"
    files_to_change ={}
    for file in glob.iglob( os.path.join(source_dir, "include", "**"), recursive=True):
      if not os.path.isdir(file):
        with open(file) as p:
          if "FARSA_PLUGIN_API" in p.read():
              files_to_change[fpath_name(file)] = fpath_rpath(file)
    #subprocess
    dir_cr =  os.path.join(build_dir, "CMakeFiles", "{}.dir".format(project_name))
    assert os.path.isdir(dir_cr), "Execute o Comando Cmake antes de rodar o build.py!"
    with open(os.path.join(dir_cr, "build.make"), "r+") as pmake:
      target = """farsapluginhelper""" #Crime
      target_cmd = []
      for line in pmake.readlines():
        if target in line:
          target_cmd = line.strip().split()
          break
      
      os.chdir(build_dir)
      p_create = subprocess.Popen(target_cmd,
                          stdout=subprocess.PIPE,
                          stderr=subprocess.PIPE)
      s = p_create.communicate()
      assert os.path.isfile('plugin_main.cpp'), "Erro pluginhelper não criou o main plugin!!!"
      with open('plugin_main.cpp', 'r+') as pcpp:
        plain_cpp = pcpp.read()
        for include in files_to_change:
          plain_cpp = plain_cpp.replace(include, files_to_change[include])
        pcpp.seek(0)
        pcpp.write(plain_cpp)
        pcpp.truncate()
      subprocess.run(['make', *make_flags])
def fpath_name(full_path):
  return full_path.split("/")[-1]

def fpath_rpath(full_path):
  chunks = full_path.split("/")
  return "/".join(chunks[chunks.index("include")+1:])