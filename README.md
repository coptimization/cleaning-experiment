
# Cleaning Experiment

Experimento de limpeza utilizando o simulador FARSA, para implementar a neuroplasticidade de comportamento em um robô de limpeza.

## Iniciando

### Pré-Requisitos
Para rodar este plugin você precisa ter instalado em seu computador:


[Cpplint](https://github.com/cpplint/cpplint) -  Static code checker for C++

[Farsa](https://sourceforge.net/p/farsa/wiki/Home/) - Framework for Autonomous Robotics Simulation and Analysis

[Cmake](https://cmake.org) - Open-source, cross-platform family of tools designed to build, test and package software.


### Instalação

Baixando o repositorio.
```
git clone https://gitlab.com/coptimization/cleaning-experiment.git
```

Depois de clonar o repositorio é necessario rodar os comandos abaixo para atualizar os submodulos do cpplint.

```
git submodule sync --recursive
git submodule update --init --recursive
```
Depois de tudo feito basta compilar seu plugin, e rodar no total99.

## Autores

* **Thales de Oliveira** - *Colaborador* - [thalesdev](https://gitlab.com/thalesdev)
* **Thiago Jansen Sampaio** - *Colaborador* - [yABSampaio](https://gitlab.com/yABSampaio)

Veja a lista de todos colaboradores do projeto [Colaboradores](https://gitlab.com/coptimization/cleaning-experiment/-/project_members).

## Licença
Este projeto é licenciado pela licensa do MIT - veja em [LICENSE.md](LICENSE.md) para mais detalhes.