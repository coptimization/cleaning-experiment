include(GNUInstallDirs)

cmake_minimum_required(VERSION 2.8)

# Setting policies, to avoid warnings
cmake_policy(VERSION 2.8)
# IF( POLICY CMP0028 ) CMAKE_POLICY( SET CMP0028 OLD ) ENDIF( POLICY CMP0028 )
if(POLICY CMP0043)
  cmake_policy(SET CMP0043 OLD)
endif(POLICY CMP0043)
if(POLICY CMP0046)
  cmake_policy(SET CMP0046 OLD)
endif(POLICY CMP0046)

project(MarxBotCleaningExperiment)

# Enabling testing INCLUDE( CTest ) ENABLE_TESTING()

# Using FARSA for generating the plugin
find_package(FARSA REQUIRED)
set(FARSA_USE_EXPERIMENTS TRUE)
include(${FARSA_USE_FILE})

# We have to increase the minimum required version if using QT5
if(FARSA_USE_QT5)
  cmake_minimum_required(VERSION 2.8.9)

  # Setting policies, to avoid warnings
  cmake_policy(VERSION 2.8.9)
  # IF( POLICY CMP0028 ) CMAKE_POLICY( SET CMP0028 OLD )
  if(POLICY CMP0043)
    cmake_policy(SET CMP0043 OLD)
  endif(POLICY CMP0043)
  if(POLICY CMP0046)
    cmake_policy(SET CMP0046 OLD)
  endif(POLICY CMP0046)
endif(FARSA_USE_QT5)

# set sources and headers files set sources from plugin on google style.
file(GLOB_RECURSE
     SW_SRCS
     ./src/*.cpp
     ./src/*/*.cxx
     ./src/*/*.c)
# set headers from plugin on google style.

file(GLOB_RECURSE
     SW_HDRS
     ./include/*.h
     ./include/*/*.hxx
     ./include/*/*.hpp
     ./include/*/*.hh)
# Add headers to sources for moc'ing up (if using Qt4)
if(NOT FARSA_USE_QT5)
  qt4_wrap_cpp(SW_SRCS
               ${SW_HDRS}
               OPTIONS
               "-nw")
endif(NOT FARSA_USE_QT5)


include_directories(./include)

# Use the ADD_FARSAPLUGIN for creating the target The plugin is a shader library
# loaded at runtime by total99
add_farsaplugin(${PROJECT_NAME} ${SW_SRCS} ${SW_HDRS})

# Also installing the plugin configuration
farsa_plugin_install_conf(${PROJECT_NAME} "conf")

# Test-related stuffs IF( BUILD_TESTING ) ### Setting the build name to a
# default value in case of tests SET( BUILDNAME "${PROJECT_NAME}" CACHE STRING
# "Name of build on the dashboard" ) MARK_AS_ADVANCED( BUILDNAME )
#
# ### Copying the file to configure CTest to the build directory CONFIGURE_FILE(
# CTestCustom.cmake ${CMAKE_BINARY_DIR}/CTestCustom.cmake COPYONLY )
#
# ### Adding the test to install the plugin ADD_FARSAPLUGIN_INSTALL_TEST()
#
# ### Adding a simple test: evolution of a couple of generations, just to check
# everything ### works as expected ADD_FARSAPLUGIN_TEST( TestEvolution 7200 test
# test evolve -t *.gen -t *.fit ) ENDIF( BUILD_TESTING )
